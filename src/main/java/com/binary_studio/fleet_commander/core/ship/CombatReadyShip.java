package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger powergridOutput;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	private PositiveInteger currentShieldHP;

	private PositiveInteger currentHullHP;

	private PositiveInteger currentCapacitorAmount;

	protected CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.currentShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.currentHullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.currentCapacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() + this.capacitorRechargeRate.value());
		if (this.currentCapacitorAmount.value() > this.capacitorAmount.value()) {
			this.currentCapacitorAmount = PositiveInteger.of(this.capacitorAmount.value());
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.currentCapacitorAmount.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}

		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());

		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {

		AttackAction damage = this.defenciveSubsystem.reduceDamage(attack);
		PositiveInteger damageReceived = PositiveInteger.of(damage.damage.value());
		PositiveInteger damageForCalculation = PositiveInteger.of(damage.damage.value());
		if (damageForCalculation.value() - this.currentShieldHP.value() > 0) {
			damageForCalculation = PositiveInteger.of(damageForCalculation.value() - this.currentShieldHP.value());
			this.currentShieldHP = PositiveInteger.of(0);
		}
		else {
			this.currentShieldHP = PositiveInteger.of(this.currentShieldHP.value() - damageForCalculation.value());
			damageForCalculation = PositiveInteger.of(0);
		}

		if (damageForCalculation.value() - this.currentHullHP.value() >= 0) {
			damageForCalculation = PositiveInteger.of(damageForCalculation.value() - this.currentHullHP.value());
			this.currentHullHP = PositiveInteger.of(0);
			return new AttackResult.Destroyed();
		}
		else {
			this.currentHullHP = PositiveInteger.of(this.currentHullHP.value() - damageForCalculation.value());
			damageForCalculation = PositiveInteger.of(0);
		}

		return new AttackResult.DamageRecived(attack.weapon, damageReceived, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		if (this.currentCapacitorAmount.value() < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {
			this.currentCapacitorAmount = PositiveInteger.of(
					this.currentCapacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
		}
		RegenerateAction regen = this.defenciveSubsystem.regenerate();
		PositiveInteger hullRegen;
		PositiveInteger shieldRegen;

		if (this.currentShieldHP.value() + regen.shieldHPRegenerated.value() > this.shieldHP.value()) {
			shieldRegen = PositiveInteger.of(this.shieldHP.value() - this.currentShieldHP.value());
			this.currentShieldHP = PositiveInteger.of(this.shieldHP.value());
		}
		else {
			shieldRegen = PositiveInteger.of(regen.shieldHPRegenerated.value());
			this.currentShieldHP = PositiveInteger.of(shieldRegen.value() + this.currentShieldHP.value());
		}

		if (this.currentHullHP.value() + regen.hullHPRegenerated.value() > this.hullHP.value()) {
			hullRegen = PositiveInteger.of(this.hullHP.value() - this.currentHullHP.value());
			this.currentHullHP = PositiveInteger.of(this.hullHP.value());
		}
		else {
			hullRegen = PositiveInteger.of(regen.hullHPRegenerated.value());
			this.currentHullHP = PositiveInteger.of(this.currentHullHP.value() + hullRegen.value());
		}

		return Optional.of(new RegenerateAction(shieldRegen, hullRegen));
	}

}
