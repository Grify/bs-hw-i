package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		if (prices == null) {
			return 0;
		}

		// Convert to Collection
		ArrayList<Integer> array = prices.collect(Collectors.toCollection(ArrayList::new));
		if (array.size() == 0) {
			return 0;
		}
		int[] counted = new int[array.size() - 1];

		// Count needed array
		for (int i = 1; i < array.size(); i++) {
			counted[i - 1] = array.get(i) - array.get(i - 1);
		}

		int answer = 0;

		// Calculate sum of numbers which bigger than zero
		for (int element : counted) {
			if (element > 0) {
				answer += element;
			}
		}

		return answer;
	}

}
