package com.binary_studio.dependency_detector;

import java.util.HashMap;
import java.util.Map;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {

		if (libraries.libraries.size() == 0 || libraries == null || libraries.libraries == null) {
			return true;
		}

		int size = libraries.libraries.size();
		int[][] matrix = new int[size][size];

		// Create an associative array
		Map<String, Integer> map = new HashMap<>();
		for (int i = 0; i < size; i++) {
			map.put(libraries.libraries.get(i), i);
		}

		// Filling the matrix
		for (int i = 0; i < libraries.dependencies.size(); i++) {
			matrix[map.get(libraries.dependencies.get(i)[0])][map.get(libraries.dependencies.get(i)[1])] = 1;
		}

		// Checking rows of the matrix for dependencies
		for (int i = 0; i < size; i++) {

			int count = 0;
			for (int j = 0; j < size; j++) {
				if (matrix[i][j] == 1) {
					count = 1;
					break;
				}
			}
			if (count == 0) {
				return true;
			}

			count = 0;
			for (int j = 0; j < size; j++) {
				if (matrix[j][i] == 1) {
					count = 1;
					break;
				}
			}
			if (count == 0) {
				return true;
			}
		}

		return false;
	}

}
